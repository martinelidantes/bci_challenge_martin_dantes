"""
Difficulty: easy
"""
#imports the necessary libraries.

from selenium import webdriver 
from bs4 import BeautifulSoup
from datetime import datetime
import pandas as pd
import pytz

#create web driver session and specify the path.
driver = webdriver.Chrome("../chromedriver.exe")

#set webpage url to extract data from
url = 'https://www.emservices.com.sg/tenders/'

#load the webpage
driver.get(url)

advert_date=[] #list to hold the advertisment dates.
closing_date=[]  #list to hold the closing dates.
client=[] #list to hold the client names.
description=[] #list to hold the client description.
eligibility=[] #list to hold the eligibility.
link=[] #list to hold the links.

#use BeautifulSoup to grab the page source, and use lxml as the parser.
soup = BeautifulSoup(driver.page_source, 'lxml')


for row in  soup.findAll('table')[0].tbody.findAll('tr'): #for loop to find all the 'tr' tags on the 'table' tag.
    
    #returns all the 1st cell in a row and put in the 'advert_date' list.
    a_date = row.findAll('td')[0].text 
    
    #creates a datetime object.
    datetime_obj = datetime.strptime(a_date, '%d/%m/%Y')
    
    #formats the extracted date, and appends it in a list named "advert_date".
    advert_date.append(datetime_obj.strftime("%m/%d/%Y %H:%M")) 
    
    #returns all the 2nd cell in a row and put in the 'closing_date' list.
    c_date = row.findAll('td')[1].text
    
     #creates a datetime object.
    datetime_obj2 = datetime.strptime(c_date, '%d/%m/%Y')
    
    #formats the extracted date, and appends it in a list named "closing_date".
    closing_date.append(datetime_obj2.strftime("%m/%d/%Y %H:%M"))
    
    #returns all the firt 3rd in a row and put in the 'client' list.
    client.append(row.findAll('td')[2].text)
    
    #returns all the 4th cell in a row and put in the 'description' list.
    description.append(row.findAll('td')[3].text)
    
    #returns all the 5th cell in a row and put in the 'advert_date' list.
    eligibility.append(row.findAll('td')[4].text)
    
    #returns the 6th cell in a row and find all the 'a' tags within this row.
    for a in row.findAll('td')[5].findAll('a',href=True): 
        
        #puts the href vaulue in the 'link' list.
        link.append(a['href']) 

# merge all the list by creating a dictionary of lists.
dict = {'advert_date': advert_date, 'closing_date': closing_date, 'client': client, 'description':description,
        'eligibility':eligibility, 'link':link}  

#convert dictionary into a dataframe.
df = pd.DataFrame(dict) 

#save dataframe into a csv file and remove all the index.
df.to_csv('output.csv', index=False) 

#close web driver session.
driver.close() 
