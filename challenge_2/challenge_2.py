"""
Difficulty: medium
"""


#imports the necessary libraries.

from selenium import webdriver 
from bs4 import BeautifulSoup
from datetime import datetime
import pandas as pd
import pytz

#create web driver session and specify the path.
driver = webdriver.Chrome("../chromedriver.exe")

#set webpage url to extract data from
url = 'https://businesspapers.parracity.nsw.gov.au/'

#load the webpage
driver.get(url)

meeting_date=[] #list to hold the meeting dates.
meeting_description=[]  #list to hold the meeting_description.
agenda_html_link=[] #list to hold the agenda_html_link.
agenda_pdf_link=[] #list to hold the agenda_pdf_link.
minutes=[] #list to hold the minutes.


#use BeautifulSoup to grab the page source, and use lxml as the parser.
soup = BeautifulSoup(driver.page_source, 'lxml')


for row in  soup.findAll('table',{"class":"bpsGridMenu"})[0].tbody.findAll('tr'): #for loop to find all the 'tr' tags on the 'table' tag.

    date =  row.findAll('td')[0] #returns the values of the 1st column.
    datef = date.get_text(separator=" ").strip() #gets the text from the td tag / the 'separator' converts the <br> into a whitespace.
    datef = datef.replace('.', ":") #normalizes the separator used in the time into ':'.
    datef = datef.upper() #normalizes case of the aphabets into uppercased. 
    dateflist = datef.split(' ', 4) # splits the string into 4 separate element and puts it on a list object.

    if(len(dateflist[0])<2): #checks if the number is 1 digit.
        date = str("{:02d}".format(int(dateflist[0]))) #adds 0 to the beginning of any single-digit number and assigns it to a 'date' str variable.
    else:
        date = dateflist[0] #assignes the number to the variable 'date' without changes.
    month = dateflist[1] #assigns the first list element into a 'month' string variable.
    year = dateflist[2] #assigns the second list element into a 'year' string variable.
    
    if(len(dateflist)<4): #detects if the extracted date doesn't have a time.
      dateflist.append('00:00PM') #adds time to normalize time.

    if(len(dateflist[3])<7): #adds 0 to the beginning of any single-digit number.
      dateflist[3] = dateflist[3].zfill(7)
    
    time = dateflist[3] #assigns the 3rd element of the list 'dateflist' to variable 'time'.
    
    timezone = pytz.timezone("Australia/Sydney") #sets the timezone.
    
    datetime_str = date+" "+month+" "+ year + " "+time #merges the variables into a string.
    datetime_obj = datetime.strptime(datetime_str, '%d %b %Y %H:%M%p') #creates a datetime object.
    with_timezone = timezone.localize(datetime_obj) #sets the timezone of the datetime object.
   
    meeting_date.append(with_timezone.strftime("%Y-%m-%d %H:%M:%S%z")) #formats the extracted date, and appends it in a list named "meeting_date".

   
    meeting_description.append(row.findAll('td')[1].contents[0].strip())#returns the values of the 2nd column, strips the unecessary sentences and appends it in a list named "meeting_description".    
    
    if(row.findAll('td')[2].findAll('a',{"class":"bpsGridHTMLLink"})): #checks if theres an html link in the tag 'td'.
        for att in row.findAll('td')[2].findAll('a',{"class":"bpsGridHTMLLink"}, href = True):
            agenda_html_link.append(url+str(att['href'])) #adds the link to the agenda_html_link list.
        
        
    else:
        agenda_html_link.append(" ") #adds blank to the list to maintain position.

    if(row.findAll('td')[2].findAll('a',{"class":"bpsGridPDFLink"})): #checks if theres an pdf link in the tag 'td'.
        for att in row.findAll('td')[2].findAll('a',{"class":"bpsGridPDFLink"}, href = True):
            agenda_pdf_link.append(url+str(att['href'])) #adds the link to the agenda_pdf_link list.
        
        
    else:
        agenda_pdf_link.append(" ") #adds blank to the list to maintain position.

    if(row.findAll('td')[4].findAll('a',{"class":"bpsGridHTMLLink"})):  #checks if theres an meeting html link in the tag 'td'.
        for att in row.findAll('td')[4].findAll('a',{"class":"bpsGridHTMLLink"}, href = True):
            minutes.append(url+str(att['href'])) #adds the link to the minutes list.
        
        
    else:
        minutes.append(" ")#adds blank to the list to maintain position.



    
dict = {'meeting_date': meeting_date, 'meeting_description': meeting_description, 'agenda_html_link': agenda_html_link,
        'agenda_pdf_link':agenda_pdf_link, 'minutes':minutes} #merges all the list into a dictionary.
      

df = pd.DataFrame(dict) #converts dictionary into a dataframe.

df.to_csv('output.csv', index=False) #save dataframe into a csv file and remove all the index.
driver.close() #close web driver session.

